# VisualizeDatabase.py
# Alexander R. Cavaliere <arc6393@rit.edu>
#

import sqlite3
import multiprocessing

from sklearn import tree
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs

import numpy
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as Axes3D

import statistics
import math

import time
import datetime

#import forecastio

_API_KEY_ =  "2cd7b3eec9ef34d1eb3342ab8fb67200"
_LAT_     =  43.1548
_LON_     = -77.6156
_C_TIME_CHANGE_ = 1472659199

class Regressor:

    # Class State
    training_data = []
    training_targets = []
    testing_data = []
    testing_targets = []

    def __init__( self, training_data, training_targets, testing_data, testing_targets ):
        self.training_data = training_data
        self.training_targets = training_targets
        self.testing_data = testing_data
        self.testing_targets = testing_targets

    def Train_Test_Tree( self ):
        dec_reg = tree.DecisionTreeRegressor( max_depth = 3 )
        dec_reg.fit( self.training_data, self.training_targets )
        with open( "winter_decision_tree_regressor.dot", 'w') as f:
            f = tree.export_graphviz( dec_reg, out_file = f )
        results = dec_reg.predict( self.testing_data )

        temps = []
        pressures = []
        dates = []
        for row in self.testing_data:
            temps.append( row[0] )
            pressures.append( row[1] )
            dates.append( row[2] )


        train_temps =[]
        train_pressures = []
        train_dates = []
        for row in self.training_data:
            train_temps.append( row[0] )
            train_pressures.append( row[1] )
            train_dates.append( row[2] )

        print( len(results ) )
        # 3D Plot
        figure = plt.figure()
        ax = figure.add_subplot( 111, projection='3d' ) # This is the recommended way to do this.
        ax.scatter( temps, pressures, results, c= 'b', s = 2, label = "Results on Testing Data" )
        ax.scatter( temps, pressures, self.testing_targets, c= 'r', s = 2, label = "Validation Data")

        ax.set_xlabel( "Temperature (F)" )
        ax.set_ylabel( "Air Pressure (mb)" )
        ax.set_zlabel( "Preferred Indoor Temperature (F)" )
        plt.title( "Air Pressure and Temperature over Time")
        plt.savefig( "3d Plot Dec Tree.png")

        # Plot What Happened
        plt.figure()
        plt.scatter( train_pressures, self.training_targets, c="darkorange", label="Training Data", s = 1)
        plt.scatter( pressures, results, c="blue", label = "Testing Data, max_depth=3", s = 1 )
        plt.scatter( pressures, self.testing_targets, c="red", label = "Validation Data", s = 1 )
        plt.xlabel( "Air Pressure Data (mb)")
        plt.ylabel( "Target Indoor Temperature (F)")
        plt.title( "Decision Tree Regressor on \nPressure Data")
        plt.legend()
        plt.savefig( "pressure_regression.png")


        # Temp Plot
        plt.figure()
        plt.scatter( train_temps, self.training_targets, c="darkorange", label="Training Data", s = 1)
        plt.scatter( temps, results, c="blue", label = "Testing Data, max_depth=3", s = 1 )
        plt.scatter( temps, self.testing_targets, c="red", label = "Validation Data", s = 1 )
        plt.xlabel( "Temperature (F)")
        plt.ylabel( "Target Indoor Temperature (F)")
        plt.title( "Decision Tree Regressor on \nTemperature Data")
        plt.legend()
        plt.savefig( "temperature_regression.png")
        return 1


    def Cluster( self ):
        training = KMeans( n_jobs = -1 )
        cluster_training = training.fit_predict( self.training_data )
        cluster_testing  = KMeans(  n_jobs = -1 ).fit_predict( self.testing_data )
        labels = training.labels_

        train_temps =[]
        train_pressures = []
        for row in self.training_data:
            train_temps.append( row[0] )
            train_pressures.append( row[1] )

        temps = []
        pressures = []
        dates = []
        for row in self.testing_data:
            temps.append( row[0] )
            pressures.append( row[1] )
            dates.append( row[2] )


        print( cluster_training )
        norm = plt.Normalize()
        colored_labels = plt.cm.viridis( norm( labels ) )
        print( colored_labels )
        colors = plt.cm.viridis( norm( cluster_training ) )
        colors_testing = plt.cm.viridis( norm( cluster_testing ) )
        color = [ "g", "r", "b", "y" ]
        # 3D Plot of the clusters over time
        figure = plt.figure()
        ax = figure.add_subplot( 111, projection='3d' ) # This is the recommended way to do this.
        for value in range( len( self.training_data ) ):
            ax.scatter( self.training_data[value][0], self.training_data[value][1], self.training_data[value][2], c = colored_labels[value], s=1 )
        ax.set_xlabel( "Temperature (F)" )
        ax.set_ylabel( "Air Pressure (mb)" )
        ax.set_zlabel( "Time (Unix Epoch)" )
        plt.title( "Air Pressure and Temperature over Time")
        plt.savefig( "3d Plot.png")
        #
        plt.figure()
        plt.scatter( train_temps, train_pressures, c=colors )
        plt.xlabel( "Temperatures (F)" )
        plt.ylabel( "Air Pressure (mb)" )
        plt.title(  "Training Set Clustering" )
        plt.savefig( "training_clusters.png" )
        plt.figure()
        plt.scatter( temps, pressures, c=colors_testing )
        plt.xlabel( "Temperatures (F)" )
        plt.ylabel( "Air Pressure (mb) ")
        plt.savefig( "testing_clusters.png" )
        return 1


class DataStorage(multiprocessing.Process):

    dictionary = None

    def __init__(self):
        self.dictionary = multiprocessing.Manager().dict()

class SeasonThread( multiprocessing.Process ):
    indoor_temps = []
    target_temps = []
    outdoor_temps = []
    dates = []
    pressures = []

    indoor_temp_mean = 0
    indoor_temp_variance = 0
    indoor_temp_sigma = 0

    outdoor_temp_mean = 0
    outdoor_temp_variance = 0
    outdoor_temp_sigma = 0

    target_temp_mean = 0
    target_temp_variance = 0
    target_temp_sigma = 0

    pressure_mean = 0
    pressure_variance = 0
    pressure_sigma = 0

    start_time = 0
    end_time = 0
    database_name = ""

    weather_time = 0
    forecast = None
    forecast_index = 0

    time_break_downs = []
    interval = 0

    training_data = []
    training_targets = []

    data_storage = None

    def __init__( self, start_time, end_time, database_name, data_storage ):
        multiprocessing.Process.__init__( self )
        self.start_time = start_time
        self.end_time = end_time
        self.database_name = database_name
        self.data_storage = data_storage

    def Cache_Weather_Data( self, current_time ):
        if self.weather_time == 0:
            self.weather_time = current_time
            self.forecast = forecastio.load_forecast( _API_KEY_, _LAT_, _LON_, time = datetime.datetime.fromtimestamp( self.weather_time ) )
        if current_time <= self.weather_time + 3600:
            self.interval += 1
        if current_time > self.weather_time + 3600:
            self.forecast_index += 1
            if len( self.forecast.hourly().data ) == self.forecast_index:
            #if 24 == self.forecast_index:
                self.time_break_downs.append( self.interval )
                self.interval = 0
                self.forecast_index = 0
                self.weather_time = current_time
                self.forecast = forecastio.load_forecast( _API_KEY_, _LAT_, _LON_, time = datetime.datetime.fromtimestamp( self.weather_time ) )

    def C_to_F( self, temp ):
        return ( temp * (9.0/5.0) ) + 32.0

    def Read_Database( self ):
        new_rows = []
        connection = sqlite3.connect( self.database_name, 120 )
        cursor = connection.cursor()
        #cursor.execute( "create table historical_data (time integer, indoor_temperature real, target_temperature real, outdoor_temperature real, pressure real);" )
        # Line below when we want to pull down more temperature data from an updated DB
        #cursor.execute( "select * from current_data where time >= " + str( self.start_time ) + " and time <= " + str( self.end_time ) + ";" )
        # Line Below will use stored weather data (Pressure included) for the graph
        cursor.execute( "select * from historical_data where time >= " + str( self.start_time ) + " and time <= " + str( self.end_time ) + ";" )
        chooser = 0
        for row in cursor:
            #print( row )
            #if row[1] != -1000: # Sensor Not Connected
            chooser += 1
            if chooser != 999:
                chooser += 1
                continue
            chooser = 0
            if row[3] != None: # Bad Data Point
                if float(row[0]) >= _C_TIME_CHANGE_: # That switch to C makes this necessary
                    self.indoor_temps.append( self.C_to_F(float(row[1]) ) )
                    self.target_temps.append( self.C_to_F( float(row[2]) ) )
                    self.outdoor_temps.append( self.C_to_F( float(row[3]) ) )
                else:
                    self.indoor_temps.append( float(row[1]) )
                    self.target_temps.append( float(row[2]) )
                    self.outdoor_temps.append( float(row[3]) )
                #self.dates.append( datetime.datetime.fromtimestamp( row[0] ) )
                self.dates.append( row[0] )
                self.pressures.append( float(row[4]) ) # We use this now that we have the data

                # Use this if we are adding more pressures to the historical records
                #self.Cache_Weather_Data( int(row[0]) )
                #self.pressures.append( self.forecast.hourly().data[self.forecast_index].pressure )
                #new_rows.append( ( int(row[0]), row[1], row[2], row[3], self.pressures[-1] ) )

        # Comment this section out once we have the data!
        '''
        with connection:
            for row in new_rows:
                cursor.execute( "insert into historical_data values (?,?,?,?,?);", row )
        connection.commit()
        '''
        connection.close()


    def Statistics( self ):
        self.indoor_temp_mean = statistics.mean( self.indoor_temps )
        self.indoor_temp_variance = statistics.variance( self.indoor_temps )
        self.indoor_temp_sigma = math.sqrt( float( self.indoor_temp_variance ) )

        self.outdoor_temp_mean = statistics.mean( self.outdoor_temps )
        self.outdoor_temp_variance = statistics.variance( self.outdoor_temps )
        self.outdoor_temp_sigma = math.sqrt( self.outdoor_temp_variance  )

        self.target_temp_mean = statistics.mean( self.target_temps )
        self.target_temp_variance = statistics.variance( self.target_temps )
        self.target_temp_sigma = math.sqrt( self.target_temp_variance )

        self.pressure_mean = statistics.mean( self.pressures )
        self.pressure_variance = statistics.variance( self. pressures )
        self.pressure_sigma = math.sqrt( self.pressure_variance )


    def Graph_Time_VS_Temperatures( self ):
        line4, = plt.plot( self.dates, self.pressures )
        line4.set_label( "Air Pressure (mb)" )

        line1, = plt.plot( self.dates, self.indoor_temps )
        line1.set_label( "Indoor Temperatures (F)" )

        line2, = plt.plot( self.dates, self.target_temps )
        line2.set_label( "Target Temperatures (F)" )

        line3, = plt.plot( self.dates, self.outdoor_temps )
        line3.set_label( "Outdoor Temperatures (F)" )

        plt.xlabel( "Date (Unix Epoch)" )
        plt.xticks( rotation = 'vertical' )
        plt.ylabel( "Temperature (F)" )
        plt.legend()

        #plt.figure( figsize=(12,10) )
        plt.title( "Temp. Values From " +\
                   datetime.datetime.fromtimestamp( self.start_time ).strftime( '%c' ) + " to \n" + datetime.datetime.fromtimestamp( self.end_time ).strftime( '%c' ) )
        plt.savefig( "Temperature Values From " +\
                     datetime.datetime.fromtimestamp( self.start_time ).strftime( '%c' ) + " to " + datetime.datetime.fromtimestamp( self.end_time ).strftime( '%c' ) )


    def Make_Decision_Tree_Data( self ):
        self.training_targets = self.target_temps
        self.training_data= list( zip( self.outdoor_temps, self.pressures, self.dates ) )


    def Save_Data( self ):
        self.data_storage.dictionary[self.start_time] = ( self.target_temps, list( zip (self.outdoor_temps, self.pressures, self.dates) ) )
        #print( self.data_storage.dictionary[self.start_time] )


    def run( self ):
        self.Read_Database()
        self.Statistics()
        self.Graph_Time_VS_Temperatures()
        self.Make_Decision_Tree_Data()
        self.Save_Data()


# Seconds to add by to move forward 3 months + 1 day 7938799
winter_start_time = 1448946000
winter_end_time = 1456808399

spring_start_time = 1456808400
spring_end_time = 1464710399

summer_start_time = 1464753600
summer_end_time = 1472659199

fall_start_time = 1472702400
fall_end_time = 1480525199

winter_start_time_2016 = 1480568400
winter_end_time_2017 = 1488344399

database_name = "pystat.db"

data_storage = DataStorage()

winter_thread = SeasonThread( winter_start_time, winter_end_time, database_name, data_storage )
winter_thread.start()

#spring_thread = SeasonThread( spring_start_time, spring_end_time, database_name )
#spring_thread.start()

#summer_thread = SeasonThread( summer_start_time, summer_end_time, database_name )
#summer_thread.start()

#fall_thread = SeasonThread( fall_start_time, fall_end_time, database_name )
#fall_thread.start()

test_thread = SeasonThread( winter_start_time_2016, winter_end_time_2017, database_name, data_storage )
test_thread.start()

while True:
    list_of_threads = multiprocessing.active_children()
    if len( list_of_threads ) <= 1:
        break
    print( list_of_threads )
    time.sleep( 1 )


winter_regressor = Regressor( data_storage.dictionary[winter_start_time][1], \
                              data_storage.dictionary[winter_start_time][0], \
                              data_storage.dictionary[winter_start_time_2016][1], \
                              data_storage.dictionary[winter_start_time_2016][0] )


winter_regressor.Train_Test_Tree()
winter_regressor.Cluster()
